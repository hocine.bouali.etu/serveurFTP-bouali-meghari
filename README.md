## Implementation d'un serveur FTP

 Bouali Hocine    
 Meghari Samy
  		
 07/03/2021

#### Introduction

Ce projet est la mise en oeuvre d'un serveur respectant le [protocole FTP](http://abcdrfc.free.fr/rfc-vf/pdf/rfc959.pdf)

Projet réalisé sous JAVA 11

#### Exigences

L'ensemble des exigences ont été respectées , seule BADP n'a pas été respecté car le serveur est un serveur Anonyme (il n'y a pas d'utilisateur hormis l'utilisateur **anonymous**) ce qui fait que si le nom d'utilisateur est correct le mot de passe l'est également

## Utilisation 

##### Etape 1: récupérer le projet

 Clone du dépôt dans le répertoire de votre choix

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/serveurFTP-bouali-meghari.git
 
 Placez-vous dans le bon dossier

	$cd serveurFTP-bouali-meghari/

##### Etape 2: exécuter le projet

 Compilation du projet 
 
	$mvn package
 
 Cela va générer un *.jar* dans le dossier */target*
 
 Exécution du projet:
 
	$java -jar target/ServeurFtp.jar

 Il est possible de renseigner le port sur lequel on veut lancer le serveur (**vaudra 21 si aucun argument n'est passé**)

	$java -jar target/ServeurFtp.jar 8080
	
 Il est possible de préciser le chemin (chemin *relatif uniquement* à partir du dossier du projet,**vaudra /serveur si aucun argument n'est passé**)
	
	$java -jar target/ServeurFTP.jar /../

 Il est également possible de préciser les deux en meme temps
	
	$java -jar target/ServeurFTP.jar /../ 8080
	

Une vidéo de démonstration est disponible dans *doc/demo.mkv*

Le diagramme UML est également disponible dans *doc/uml.png* et *doc/commande.png*

# Architecture

Pour la gestion des différentes commandes, nous avons décidé  d'utiliser le design pattern Factory 

Nous avons donc une classe abstraite *Commande*. L'implementation de toutes les commandes hériterons de cette classe


##### Gestion d'erreur

En cas de levée d'exception, le serveur et/ou client ne crash pas, un message avec un code d'erreur est envoyé

```
try {
	Files.move(oldp, newp);
	Utils.sendCommand(client.getPrinter(), "250 rename successful.");
} catch (IOException e) {
	Utils.sendCommand(client.getPrinter(), "550 failed to rename.");
}
```

	
# Code Samples

Methode permettant à un client de se connecter

```
public void run() {
	try {
		in = socket.getInputStream();
		reader = new BufferedReader(new InputStreamReader(in));
		out = socket.getOutputStream();
		printer = new PrintWriter(out, true);
		new Connexion(printer);
		String line;
		while ((line = reader.readLine()) != null) {
			Commande cf = CommandeFactory.CmdFactory(line);
			cf.requete(line, this);
		}
	} catch (IOException e) {
		// le serveur continue de tourner pour les autres clients
	}
}

```


 Méthode qui correspond à l'implementation de la commande *LIST*, en cas d'exception la méthode envoie au client un message d'erreur ( le serveur ne crash pas )


```
@Override
public void requete(String message, Client client) {
	Utils.sendCommand(client.getPrinter(), "150 Here comes the directory listing");
	try {
		data.filesList();
		Utils.sendCommand(client.getPrinter(), "226 Directory send ok");
	} catch (IOException e) {
		Utils.sendCommand(client.getPrinter(), "550 Failed to send the Directory.");
	}
}
```



