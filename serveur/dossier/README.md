## Implementation d'un serveur FTP

 Bouali Hocine    
 Meghari Samy
  		
 05/03/2021

#### Introduction

Ce projet est l'implementation d'un serveur respectant le [protocole FTP](http://abcdrfc.free.fr/rfc-vf/pdf/rfc959.pdf)



## Utilisation 

##### Etape 1: récupérer le projet

 Clone du dépôt dans le répertoire de votre choix

	$git clone https://gitlab.univ-lille.fr/hocine.bouali.etu/serveurFTP-bouali-meghari.git
 
 Placez-vous dans le bon dossier

	$cd serveurFTP-bouali-meghari/

##### Etape 2: exécuter le projet

 Compilation du projet 
 
	$mvn package
 
 Cela va générer un *.jar* dans le dossier */target*
 
 Exécution du projet:
 
	$java -jar target/ServeurFtp.jar

 Il est possible de renseigner le port sur lequel on veut lancer le serveur (**vaudra 21 si aucun ou plus d'un argument est passé**)

	$java -jar target/ServeurFtp.jar 8080
	
	
Une vidéo de démonstration est disponible dans *doc/demo.mkv*

Le diagramme UML est également disponible dans *doc/uml.png*

# Architecture

##### Gestion d'erreur

	
# Code Samples



