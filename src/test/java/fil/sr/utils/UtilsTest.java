package fil.sr.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testGetCMD() {
		String msg = "CWD /toto/titi";
		String res = Utils.getCMD(msg);
		assertEquals("CWD", res);
	}

	@Test
	public void testGetCMDWithoutMsg() {
		String msg = "CWD ";
		String res = Utils.getCMD(msg);
		System.out.println(res);
		assertEquals("CWD", res);
	}

	@Test
	public void testGetMSGOfCMD() {
		String msg = "CWD /toto/titi";
		String res = Utils.getMSGOfCMD(msg);
		assertEquals("/toto/titi", res);
	}

	@Test
	public void testGetip() {
		String cmdPORT = "89,133,150,29,73,26";
		String ip = Utils.getip(cmdPORT);
		assertTrue(ip.equals("89.133.150.29"));
	}

	@Test
	public void testGetPort() {
		String cmdPORT = "89,133,150,29,73,26";
		int port = Utils.getPort(cmdPORT);
		assertTrue(port == (73 * 256) + 26);
	}

}
