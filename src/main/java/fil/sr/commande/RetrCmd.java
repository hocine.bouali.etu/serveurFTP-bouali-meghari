package fil.sr.commande;

import java.io.IOException;

import fil.sr.Client;
import fil.sr.utils.Type;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande RETR
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class RetrCmd extends Commande {

	/**
	 * Permet au client de telecharger un fichier disponible sur le serveur
	 */
	@Override
	public void requete(String message, Client client) {
		try {
			String filename = Utils.getMSGOfCMD(message);
			if (client.getType() == Type.BINARY) {
				Utils.sendCommand(client.getPrinter(), "150 Opening BINARY mode data connection for " + filename);
				data.sendFileBinary(filename);
				Utils.sendCommand(client.getPrinter(), "226 Transfer complete.");
			} else if (client.getType() == Type.ASCII) {
				Utils.sendCommand(client.getPrinter(), "150 Opening ASCII mode data connection for " + filename);
				data.sendFileASCII(filename);
				Utils.sendCommand(client.getPrinter(), "226 Transfer complete.");
			}
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "552 Failed to tranfer.");
		}
	}
}
