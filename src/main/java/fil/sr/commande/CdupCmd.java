package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande CDUP
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class CdupCmd extends Commande {

	/**
	 * Change le repertoire courant vers le repertoire parent. S'il s'agit du
	 * repertoire racine, il est impossible de remonter plus haut. Envoie au client
	 * un message 200 si le changement a reussi
	 */
	@Override
	public void requete(String message, Client client) {
		int indexfirst = client.getPath().indexOf("/");
		int indexlast = client.getPath().lastIndexOf("/");
		if (indexfirst == indexlast) {
			client.setPath("/");
		} else {
			String newPath = client.getPath().substring(0, indexlast);
			client.setPath(newPath);
		}

		Utils.sendCommand(client.getPrinter(), "200 directory succesfully changed");
	}

}
