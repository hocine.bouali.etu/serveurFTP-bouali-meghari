package fil.sr.commande;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande RMD
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class RmdCmd extends Commande {

	/**
	 * Permet du supprimer un dossier
	 */
	@Override
	public void requete(String message, Client client) {
		String filename = Utils.getMSGOfCMD(message);
		String path = "."+client.getRoot() + client.getPath() + "/" + filename;
		Path p = Paths.get(path);
		try {
			Files.delete(p);
			Utils.sendCommand(client.getPrinter(), "250 Remove directory operation successful.");
		} catch (NoSuchFileException e) {
			Utils.sendCommand(client.getPrinter(), "530 Directory not found.");
		} catch (IOException e1) {
			Utils.sendCommand(client.getPrinter(), "550 Permission denied.");
		}

	}

}
