package fil.sr.commande;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande RNFR
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class RnfrCmd extends Commande {

	/**
	 * Permet de renommer un fichier
	 */
	@Override
	public void requete(String message, Client client) {
		String filename = Utils.getMSGOfCMD(message);
		String path = "."+client.getRoot() + client.getPath() + "/" + filename;
		Path p = Paths.get(path);
		if (Files.exists(p)) {
			Utils.sendCommand(client.getPrinter(), "350 Ready for RNTO.");
			fileToRename = filename;
		} else {
			Utils.sendCommand(client.getPrinter(), "550 Failed to prepare RNTO.");
		}
	}

}
