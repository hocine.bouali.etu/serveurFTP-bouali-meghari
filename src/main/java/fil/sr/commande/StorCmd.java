package fil.sr.commande;

import java.io.IOException;

import fil.sr.Client;
import fil.sr.utils.Type;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande STOR
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class StorCmd extends Commande {

	/**
	 * Permet au client de mettre en ligne un fichier sur le serveur
	 */
	@Override
	public void requete(String message, Client client) {
		String filename = Utils.getMSGOfCMD(message);
		try {
			Utils.sendCommand(client.getPrinter(), "150 Ok to send data");
			if (client.getType() == Type.BINARY) {

				data.uploadFileBinary(filename);

			} else if (client.getType() == Type.ASCII) {
				data.uploadFileASCII(filename);

			}
			Utils.sendCommand(client.getPrinter(), "226 Transfer completed");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "552 Failed to tranfer");
		}

	}

}
