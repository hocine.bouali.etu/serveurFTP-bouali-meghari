package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande USER
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class UserCmd extends Commande {

	@Override
	public void requete(String message, Client client) {
		if (message.equals("USER anonymous")) {
			Utils.sendCommand(client.getPrinter(), "331 Please specify the password");

		} else {
			Utils.sendCommand(client.getPrinter(), "530 This FTP server is anonymous only");

		}

	}

}
