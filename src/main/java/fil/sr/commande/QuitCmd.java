package fil.sr.commande;

import java.io.IOException;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande QUITs
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class QuitCmd extends Commande {

	/**
	 * Deconnecte le client du serveur
	 */
	@Override
	public void requete(String message, Client client) {

		try {
			client.getSocket().close();
			Utils.sendCommand(client.getPrinter(), "221 Goodbye.");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "500 Error while QUIT.");
		}
	}

}
