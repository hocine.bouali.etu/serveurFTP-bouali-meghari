package fil.sr.commande;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande DELE
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class DeleCmd extends Commande {

	/**
	 * Suppression d'un fichier. Envoie au client le code 250 si la suppression a
	 * reussie, 530 si le fichier n'existe pas , 550 si l'operation a echouer
	 */
	@Override
	public void requete(String message, Client client) {
		String filename = Utils.getMSGOfCMD(message);
		String path = "." +client.getRoot()+ client.getPath() + "/" + filename;
		Path p = Paths.get(path);
		try {
			Files.delete(p);
			Utils.sendCommand(client.getPrinter(), "250 Delete operation successful.");
		} catch (NoSuchFileException e) {
			Utils.sendCommand(client.getPrinter(), "530 File not found.");
		} catch (IOException e1) {
			Utils.sendCommand(client.getPrinter(), "550 Permission denied.");
		}
	}

}
