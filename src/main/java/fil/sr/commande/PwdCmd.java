package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande PWD
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class PwdCmd extends Commande {

	/**
	 * Renvoie le repertoire courant
	 */
	@Override
	public void requete(String message, Client client) {

		Utils.sendCommand(client.getPrinter(), "257 \"" + client.getPath() + "\" is the current directory");
	}

}
