package fil.sr.commande;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande RNTO
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class RntoCmd extends Commande {

	/**
	 * Permet de renommer un fichier
	 */
	@Override
	public void requete(String message, Client client) {
		String newFilename = Utils.getMSGOfCMD(message);
		String oldFilename = fileToRename;
		String path = "." + client.getRoot() + client.getPath() + "/" + oldFilename;

		String newPath;
		if (newFilename.startsWith("/")) {
			newPath = "." + client.getRoot() + newFilename;
		} else {
			newPath = "." + client.getRoot() + client.getPath() + "/" + newFilename;
		}

		Path oldp = Paths.get(path);
		Path newp = Paths.get(newPath);

		try {
			Files.move(oldp, newp);
			Utils.sendCommand(client.getPrinter(), "250 rename successful.");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "550 failed to rename.");
		}
	}

}
