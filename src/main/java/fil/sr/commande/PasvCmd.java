package fil.sr.commande;

import java.io.IOException;
import java.net.ServerSocket;

import fil.sr.Client;
import fil.sr.Data;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande PASV
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class PasvCmd extends Commande {

	private ServerSocket ss;

	/**
	 * Activation du mode Passif (PASV). Envoie au client le code d'erreur 227 en
	 * cas de reussite ainsi que le nouveau port d'écoute pour le canal de données,
	 * 530 sinon
	 */
	@Override
	public void requete(String message, Client client) {
		try {
			ss = new ServerSocket(0);
			int port = ss.getLocalPort();
			int p1 = port / 256;
			int p2 = port - p1 * 256;
			Utils.sendCommand(client.getPrinter(), "227 Entering Passive Mode (127,0,0,1," + p1 + "," + p2 + ")");
			super.socketData = ss.accept();
			data = new Data(socketData, client);

		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "530 Failed to enter in Passive mode.");
		}

	}

}
