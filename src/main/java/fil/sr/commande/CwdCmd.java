package fil.sr.commande;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande CWD
 * @author Bouali Hocine et meghari Samy
 *
 */
public class CwdCmd extends Commande {

	/**
	 * Change le repertoire courant vers un nouveau repertoire. Envoie du message
	 * 250 si le changement a reussi , 550 sinon
	 */
	@Override
	public void requete(String message, Client client) {

		String newPath = Utils.getMSGOfCMD(message);
		if (newPath.startsWith("/")) {
			client.setPath(newPath);
		}
		else {
			client.setPath(client.getPath() + "/" + newPath);
		}

		Path p = Paths.get("." +client.getRoot()+ client.getPath());
		
		if (Files.exists(p)) {
			Utils.sendCommand(client.getPrinter(), "250 Directory successfully changed");
		} else {
			Utils.sendCommand(client.getPrinter(), "550 failed to changed directory");
		}

	}

}
