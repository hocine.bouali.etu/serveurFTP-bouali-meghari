package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande SYST
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class SystCmd extends Commande {

	/**
	 * Permet de connaitre le systeme d'exploitation sur lequel repose le serveur
	 */
	@Override
	public void requete(String message, Client client) {
		String syst = "215 " + System.getProperty("os.name") + " " + System.getProperty("os.version") + " "
				+ System.getProperty("os.arch");
		Utils.sendCommand(client.getPrinter(), syst);

	}

}
