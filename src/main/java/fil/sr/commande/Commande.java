package fil.sr.commande;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import fil.sr.Client;
import fil.sr.Data;

/**
 * Classe abstraite des differentes commandes FTP
 * @author Bouali Hocine et meghari Samy
 *
 */
public abstract class Commande {
	protected ServerSocket ssData;
	protected Socket socketData;
	protected PrintWriter writerData;
	protected static Data data;
	protected static String fileToRename;

	/**
	 * @param message La commande envoyé par le client
	 * @param client  Le client connecté au Serveur
	 */
	public abstract void requete(String message, Client client);

}
