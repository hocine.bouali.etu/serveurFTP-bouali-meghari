package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Reponse à une commande non traitée
 * @author Bouali Hocine et Meghari samy
 *
 */
public class NotImplementedCmd extends Commande{

	/**
	 * Renvoie d'un message dans le cas d'une commande non implementé
	 */
	@Override
	public void requete(String message, Client client) {
		Utils.sendCommand(client.getPrinter(), "502 Not implemented yet.");
		
	}

}
