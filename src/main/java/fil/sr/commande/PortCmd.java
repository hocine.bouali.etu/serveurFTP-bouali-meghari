package fil.sr.commande;

import java.io.IOException;
import java.net.Socket;

import fil.sr.Client;
import fil.sr.Data;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande PORT
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class PortCmd extends Commande {

	/**
	 * Activation du mode Actif (PORT) et creation d'un canal de données sur le port
	 * envoyé par le client. Envoie au client le code d'erreur 200 en cas de
	 * reussite, 421 sinon
	 */
	@Override
	public void requete(String message, Client client) {
		String tmp = Utils.getMSGOfCMD(message);// on recupere le message sous la forme h1,h2,h3,h4,p1,p2

		String ip = Utils.getip(tmp);
		int newPort = Utils.getPort(tmp);
		try {
			super.socketData = new Socket(ip, newPort);// on creer notre socket de donnees
			data = new Data(socketData, client);
			Utils.sendCommand(client.getPrinter(), "200 Entering Active Mode .");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "421 Can't open data connection.");
		}
	}

}
