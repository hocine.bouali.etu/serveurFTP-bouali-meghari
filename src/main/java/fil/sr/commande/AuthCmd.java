package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande AUTH
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class AuthCmd extends Commande {

	/**
	 * Methode d'authentification TLS et SSL ne sont pas prise en compte. Envoie du
	 * code d'erreur 530 au client
	 */
	@Override
	public void requete(String message, Client client) {
		Utils.sendCommand(client.getPrinter(), "530 Please login with USER and PASS");

	}

}
