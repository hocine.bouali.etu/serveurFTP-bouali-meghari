package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Type;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande TYPE
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class TypeCmd extends Commande {

	/**
	 * Permet de choisir le mode de communication ( ASCII ou BINARY)
	 */
	@Override
	public void requete(String message, Client client) {
		switch (Utils.getMSGOfCMD(message)) {
		case "I":
			client.setType(Type.BINARY);
			Utils.sendCommand(client.getPrinter(), "200 switching to BINARY mode");
			break;
		case "A":
			client.setType(Type.ASCII);
			Utils.sendCommand(client.getPrinter(), "200 switching to ASCII mode");
			break;
		default:
			Utils.sendCommand(client.getPrinter(), "504 not implemented.");
			break;
		}

	}

}
