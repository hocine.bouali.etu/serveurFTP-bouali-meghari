package fil.sr.commande;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande MKD
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class MkdCmd extends Commande {

	/**
	 * Creation d'un nouveau dossier
	 */
	@Override
	public void requete(String message, Client client) {
		String newDirName = Utils.getMSGOfCMD(message);
		String path = "." +client.getRoot()+ client.getPath() + "/" + newDirName;
		Path p = Paths.get(path);
		try {
			Files.createDirectories(p);
			Utils.sendCommand(client.getPrinter(), "257 " + client.getPath() + "/" + newDirName + " created.");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "550 Failed to create  directory.");
		}

	}

}
