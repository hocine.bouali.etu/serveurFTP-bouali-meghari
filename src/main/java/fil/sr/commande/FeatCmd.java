package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande FEAT
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class FeatCmd extends Commande {

	/**
	 * Envoie les commandes que supporte le serveur
	 */
	@Override
	public void requete(String message, Client client) {
		Utils.sendCommand(client.getPrinter(), "211-Features:\r\n211 End");

	}

}
