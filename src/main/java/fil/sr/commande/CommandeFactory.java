package fil.sr.commande;

/**
 * @author Bouali Hocine et meghari Samy
 *
 */
public class CommandeFactory {

	public static Commande CmdFactory(String commande) {
		String tmp = commande.split(" ")[0];
		switch (tmp) {
		case "AUTH":
			return new AuthCmd();
		case "USER":
			return new UserCmd();
		case "PASS":
			return new PassCmd();
		case "SYST":
			return new SystCmd();
		case "FEAT":
			return new FeatCmd();
		case "PWD":
			return new PwdCmd();
		case "TYPE":
			return new TypeCmd();
		case "PASV":
			return new PasvCmd();
		case "PORT":
			return new PortCmd();
		case "LIST":
			return new ListCmd();
		case "CWD":
			return new CwdCmd();
		case "RETR":
			return new RetrCmd();
		case "CDUP":
			return new CdupCmd();
		case "STOR":
			return new StorCmd();
		case "DELE":
			return new DeleCmd();
		case "RMD":
			return new RmdCmd();
		case "RNFR":
			return new RnfrCmd();
		case "RNTO":
			return new RntoCmd();
		case "MKD":
			return new MkdCmd();
		case "QUIT":
			return new QuitCmd();
		default:
			return new NotImplementedCmd();
		}
	}
}
