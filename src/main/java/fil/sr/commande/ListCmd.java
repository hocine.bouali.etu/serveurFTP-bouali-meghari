package fil.sr.commande;

import java.io.IOException;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande LIST
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class ListCmd extends Commande {

	/**
	 * liste le contenu du repertoire courant
	 */
	@Override
	public void requete(String message, Client client) {
		Utils.sendCommand(client.getPrinter(), "150 Here comes the directory listing");

		try {
			data.filesList();
			Utils.sendCommand(client.getPrinter(), "226 Directory send ok");
		} catch (IOException e) {
			Utils.sendCommand(client.getPrinter(), "550 Failed to send the Directory.");
		}

	}

}
