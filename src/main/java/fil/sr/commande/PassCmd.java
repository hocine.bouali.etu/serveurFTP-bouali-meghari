package fil.sr.commande;

import fil.sr.Client;
import fil.sr.utils.Utils;

/**
 * Implementation de la commande PASS
 * 
 * @author Bouali Hocine et meghari Samy
 *
 */
public class PassCmd extends Commande {

	/**
	 *
	 */
	@Override
	public void requete(String message, Client client) {
		Utils.sendCommand(client.getPrinter(), "230 Login successful");

	}

}
