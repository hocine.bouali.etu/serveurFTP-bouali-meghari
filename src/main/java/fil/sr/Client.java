package fil.sr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import fil.sr.commande.Commande;
import fil.sr.commande.CommandeFactory;
import fil.sr.utils.Type;

/**
 * Classe qui represente un client FTP
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Client implements Runnable {
	private Socket socket;
	private InputStream in;
	private BufferedReader reader;
	private OutputStream out;
	private PrintWriter printer;
	private String path = "/";
	private String root;
	

	private Type type = Type.ASCII;

	public Client(Socket socket,String root) {
		this.socket = socket;
		this.root = root;
	}

	@Override
	public void run() {
		try {
			in = socket.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));

			out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			new Connexion(printer);
			String line;
			while ((line = reader.readLine()) != null) {
				Commande cf = CommandeFactory.CmdFactory(line);
				cf.requete(line, this);
			}
		} catch (IOException e) {
			
		}

	}

	public Socket getSocket() {
		return socket;
	}

	public PrintWriter getPrinter() {
		return printer;
	}

	public BufferedReader getReader() {
		return reader;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	public String getRoot() {
		return root;
	}
}
