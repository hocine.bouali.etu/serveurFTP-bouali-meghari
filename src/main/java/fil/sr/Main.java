package fil.sr;

import java.io.IOException;

/**
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Main {

	public static void usage() {
		System.out.println("usage: java -jar target/ServeurFTP.jar [root] [port]");
		System.out.println(
				"Si aucun argument n'est donner, le port 21 est choisi par defaut et le serveur se lancera sur ./serveur");
		System.out
				.println("Si un argument est donner et qu'il s'agit d'un entier numerique, cela correspondra au port ");
		System.out.println("Si un argument est donner et qu'il s'agit d'une chaine, cela correspondra au root ");
		System.out.println(
				"Si deux arguments sont donner, le premier argument correspond au root et le deuxieme au port");
		System.out.println("Exemple d'utilisation : ");
		System.out.println("\t java -jar target/ServeurFTP.jar ");
		System.out.println("\t java -jar target/ServeurFTP.jar /..");
		System.out.println("\t java -jar target/ServeurFTP.jar 8080");
		System.out.println("\t java -jar target/ServeurFTP.jar /../.. 8080");

	}

	public static void main(String[] args) throws IOException {
		ServeurFTP s;
		String root = "/serveur";
		int port;
		if (args.length == 0) {
			s = new ServeurFTP(21, root);
			s.connect();
		} else if (args.length == 1) {
			port = 21;
			try {
				port = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				root = args[0];
			}
			s = new ServeurFTP(port, root);
			s.connect();

		} else if (args.length == 2) {
			try {
				root = args[0];
				port = Integer.parseInt(args[1]);
				s = new ServeurFTP(port, root);
				s.connect();
			} catch (NumberFormatException e) {
				usage();
			}
		} else {
			usage();
		}
	}
}
