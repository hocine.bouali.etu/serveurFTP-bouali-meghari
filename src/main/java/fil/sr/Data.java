package fil.sr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import fil.sr.utils.Utils;

/**
 * Classe representant le Canal de données du serveur FTP
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Data {
	private Socket socketData;
	private Client client;

	public Data(Socket socketData, Client client) {
		this.socketData = socketData;
		this.client = client;
	}

	/**
	 * Methode qui permet d'envoyer sur le serveur l'ensemble du contenu du dossier
	 * courant
	 * 
	 * @throws IOException Une erreur lors de la recuperation des données du
	 *                     repertoire
	 * 
	 */
	public void filesList() throws IOException {
		OutputStream out = socketData.getOutputStream();
		PrintWriter printerData = new PrintWriter(out, true);
		String dir = "." + client.getRoot() + client.getPath();
		List<String> contenu = Utils.listFilesUsingDirectoryStream(dir);
		for (String ctn : contenu) {
			Utils.sendCommand(printerData, ctn);
		}
		this.socketData.close();
	}

	/**
	 * Methode qui permet de telecharger un fichier disponible sur le serveur
	 * 
	 * @param filename un nom de fichier
	 * @throws IOException une erreur lors de l'ecriture du fichier
	 */
	@SuppressWarnings("resource")
	public void sendFileASCII(String filename) throws IOException {
		String path = "." + client.getRoot() + client.getPath() + "/" + filename;
		File file = new File(path);

		BufferedReader in = new BufferedReader(new FileReader(file));
		OutputStream out = socketData.getOutputStream();
		PrintWriter printerData = new PrintWriter(out, true);
		String line;
		while ((line = in.readLine()) != null) {
			Utils.sendCommand(printerData, line);
		}
		this.socketData.close();

	}

	/**
	 * Methode qui permet de telecharger un fichier disponible sur le serveur en
	 * mode binaire
	 * 
	 * @param filename Un nom de fichier
	 * @throws IOException une erreur lors de l'ecriture du fichier
	 */
	public void sendFileBinary(String filename) throws IOException {
		String path = "." + client.getRoot() + client.getPath() + "/" + filename;
		Path p = Paths.get(path);
		OutputStream out = socketData.getOutputStream();
		byte[] data = Files.readAllBytes(p);
		out.write(data);
		out.flush();
		this.socketData.close();

	}

	/**
	 * Methode qui permet de mettre en ligne sur le serveur le fichier passer en
	 * parametre en mode ASCII
	 * 
	 * @param filename Un nom de fichier
	 * @throws IOException erreur lors de l'ecriture du fichier
	 */
	public void uploadFileASCII(String filename) throws IOException {
		String path = "." + client.getRoot() + client.getPath() + "/" + filename;
		InputStream in = socketData.getInputStream();
		BufferedReader readerData = new BufferedReader(new InputStreamReader(in));
		Path p = Paths.get(path);
		String line;
		String content = "";
		while ((line = readerData.readLine()) != null) {
			content += line + "\r\n";

		}
		Files.writeString(p, content);
		this.socketData.close();

	}

	/**
	 * Methode qui permet de mettre en ligne sur le serveur le fichier passer en
	 * parametre en mode Binaire
	 * 
	 * @param filename Un nom de fichier
	 * @throws IOException une erreur lors de l'ecriture du fichier
	 */
	public void uploadFileBinary(String filename) throws IOException {
		String path = "." + client.getRoot() + client.getPath() + "/" + filename;
		InputStream in = socketData.getInputStream();
		Path p = Paths.get(path);
		byte buf[] = in.readAllBytes();
		Files.write(p, buf);
		this.socketData.close();

	}

}
