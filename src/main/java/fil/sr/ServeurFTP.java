package fil.sr;

import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;


/**
 * Classe qui permet de lancer le serveur et de connecter les differents clients
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class ServeurFTP {
	private ServerSocket ss;
	
	private int port;
	private String root;
	public ServeurFTP(int port,String root) {
		this.port = port;
		this.root = root;
	}

	/**
	 * Methode qui permet à chaque client de se connecter au serveur
	 * 
	 * @throws IOException Si le serveur n'a pas pu etre lancer
	 */
	public void connect() throws IOException {
		ss = new ServerSocket(this.port);
		System.out.println("Serveur connecté sur le port " + port);
		System.out.println("root : " + root);
		while (true) {
			Socket socket = ss.accept();
			Client client = new Client(socket,this.root);
			Thread thread = new Thread(client);
			thread.start();
		}

	}

}
