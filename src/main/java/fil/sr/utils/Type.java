package fil.sr.utils;

/**
 * Enum representant les differents types de transfert possible sur le Serveur FTP
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public enum Type {
	ASCII, BINARY;
}
