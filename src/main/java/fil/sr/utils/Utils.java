package fil.sr.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui contient des methodes permettant la bonne implementation du
 * Serveur FTP
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Utils {

	/**
	 * @param printer un printer lié à un socket sur lequel on veux envoyer des
	 *                données
	 * @param command la commande à envoyer
	 */
	public static void sendCommand(PrintWriter printer, String command) {
		printer.println(command);
	}

	/**
	 * @param reader Un reader lié à une socket sur lequel on attend une reponse
	 * @return Une ligne du reader
	 * @throws IOException
	 */
	public static String receiveCommand(BufferedReader reader) throws IOException {
		return reader.readLine();
	}

	/**
	 * Renvoie les differentes informations sur le fichier : Les droites d'acces, le
	 * nom , la taille ect
	 * 
	 * @param file un fichier
	 * @return une chaine contenant les informations du fichier
	 */
	public static String constructOneFile(Path file) {
		String r, w, x;
		String type;
		String group = "ftp";
		FileTime date;
		String name;
		String owner = "ftp";
		Long size;
		int nbItem;
		String builder = null;
		BasicFileAttributes attr;

		try {
			attr = Files.readAttributes(file, BasicFileAttributes.class);

			date = attr.lastModifiedTime();
			if (attr.isDirectory()) {
				type = "d";
				nbItem = 10;// TODO recuperer le nombre d'item

			} else if (attr.isSymbolicLink()) {
				type = "l";
				nbItem = 1;
			} else {
				type = "-";
				nbItem = 1;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy");
			x = Files.isExecutable(file) ? "x" : "-";
			w = Files.isWritable(file) ? "w" : "-";
			r = Files.isReadable(file) ? "r" : "-";
			size = attr.size();
			name = file.getFileName().toString();
			builder = type + r + w + x + r + w + x + r + w + x + " " + nbItem + " " + owner + " " + group + " " + size
					+ " " + sdf.format(date.toMillis()) + " " + name;
		} catch (IOException e) {

			throw new RuntimeException(e);
		}

		return builder;

	}

	/**
	 * @param dir un repertoire
	 * @return une liste de description de fichier/dossier contenu dans le
	 *         repertoire passer en parametre
	 * @throws IOException
	 */
	public static List<String> listFilesUsingDirectoryStream(String dir) throws IOException {
		List<String> fileList = new ArrayList<String>();

		DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir));
		for (Path path : stream) {

			fileList.add(constructOneFile(path));

		}

		return fileList;
	}

	/**
	 * Renvoie uniquement le nom de la commande
	 * 
	 * @param message La commande du client
	 * @return le nom d'une commande FTP
	 */
	public static String getCMD(String message) {
		return message.substring(0, message.indexOf(" "));
	}

	/**
	 * renvoie uniquement le parametre de la commande FTP envoyé par le client
	 * 
	 * @param message La commande du client
	 * @return Parametre de la commande
	 */
	public static String getMSGOfCMD(String message) {

		return message.substring(message.indexOf(" ") + 1, message.length());
	}

	/**
	 * Methode qui parse le retour de la commande PASV et recupere l'adresse IP
	 * 
	 * @param line retour de la commande FTP PASV nettoye, la ligne ne contient que
	 *             ce qu'il y'a entre parentheses
	 * @return l'adresse IP reconstitue
	 */
	public static String getip(String line) {
		String[] tab = line.split(",");
		return tab[0] + "." + tab[1] + "." + tab[2] + "." + tab[3];
	}

	/**
	 * Methode qui parse le retour de la commande PASV nettoye du debut de la chaine
	 * et calcule le nouveau port
	 * 
	 * @param line retour de la commande PASV nettoye , la ligne ne contient que ce
	 *             qu'il y'a entre parenthese
	 * @return le nouveau port
	 */
	public static int getPort(String line) {
		String[] tab = line.split(",");
		return Integer.parseInt(tab[4]) * 256 + Integer.parseInt(tab[5]);
	}
}
