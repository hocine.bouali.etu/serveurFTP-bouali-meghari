/**
 * Les classes du package Utils permettent de faciliter la réutilisation de
 * methodes communes
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
package fil.sr.utils;