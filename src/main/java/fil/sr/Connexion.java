package fil.sr;

import java.io.PrintWriter;

import fil.sr.utils.Utils;

/**
 * Classe qui correspond au debut de connexion entre le serveur et un client, il
 * s'agit du message d'accueil du serveur
 * 
 * @author Bouali Hocine et Meghari Samy
 *
 */
public class Connexion {
	private PrintWriter printer;

	public Connexion(PrintWriter printer) {
		this.printer = printer;

		connect();
	}

	/**
	 * Premier message envoyé par le serveur à une demande de connexion de la part d'un client
	 */
	private void connect() {
		Utils.sendCommand(printer, "220 Connexion au serveur reussie");
	}
}
